# WebCore
The web micro-framework used by web-enabled
DistriCraft projects.

## License
Licensed under the MIT license (see LICENSE for more info)

Copyright (c) DistriCraft <https://www.districraft.org>

Copyright (c) contributors