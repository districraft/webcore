/*
 * This file is  licensed under the MIT License (MIT).
 *
 * Copyright (c) DistriCraft <https://www.districraft.org>
 * Copyright (c) contributors
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package org.districraft.webcore;

import org.districraft.webcore.http.JettyServer;
import org.districraft.webcore.sql.SqlConnectionFactory;
import org.districraft.webcore.util.ShutdownThread;
import org.jooq.DSLContext;
import org.slf4j.Logger;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Properties;

/**
 * The main class that should be initialized to use WebCore
 *
 * @author "Jakub Sycha (utf_8x@districraft.org)"
 */
public class WebCore {
    protected DSLContext sqlContext;

    private String propertiesPath = "webcore.properties";
    private Properties props = new Properties();
    private Logger log;
    private String mysql_host, mysql_username, mysql_password, mysql_database;
    private int mysql_port;
    private int web_port;

    public WebCore(Logger log) throws Exception {
        this(log, null);
    }

    /**
     * @param log SLF4j logger instance
     * @throws Exception
     */
    public WebCore(Logger log, String properties) throws Exception {
        this.log = log;

        if(properties != null) this.propertiesPath = properties;

        log.info("===========================================");
        log.info("          Welcome to WebCore 1.0!          ");
        log.info("===========================================");
        log.info("");
        log.info("Initializing WebCore...");

        if(!loadPropeties()) throw new Exception("Could not load configuration. WebCore will exit");

        SqlConnectionFactory factory = new SqlConnectionFactory();
        sqlContext = factory.host(mysql_host)
                .username(mysql_username)
                .password(mysql_password).database(mysql_database)
                .port(mysql_port)
                .build();

        log.info("Adding shutdown hooks...");
        Runtime.getRuntime().addShutdownHook(new ShutdownThread(factory));
        log.info("Adding shutdown hooks... DONE");
        log.info("Initializing WebCore... DONE");
    }

    /**
     * Parse the webcore.properties file
     * @return
     */
    private boolean loadPropeties() {
        InputStream input = null;

        log.info("Loading webcore.properties...");

        try {
            input = this.getClass().getClassLoader().getResourceAsStream(this.propertiesPath);

            if(input == null) throw new FileNotFoundException("Could not find file webcore.properties");

            props.load(input);

            mysql_host = props.getProperty("mysql_host");
            mysql_username = props.getProperty("mysql_user");
            mysql_password = props.getProperty("mysql_pass");
            mysql_database = props.getProperty("mysql_db");
            mysql_port = Integer.parseInt(props.getProperty("mysql_port"));
            web_port = Integer.parseInt(props.getProperty("web_port"));

        } catch (FileNotFoundException x) {
            log.error(String.format("FileNotFoundException: %s", x.getMessage()));
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        log.info("Loading webcore.properties... DONE");
        return true;
    }

    /**
     * Start the WebCore embedded Jetty server
     * @param routes route map
     * @throws Exception
     */
    public void start(Map<String, Class<?>> routes) throws Exception {
        log.info("Starting Jetty HTTP server with WebCore...");
        new JettyServer().start(web_port, routes);
        log.info("Starting Jetty HTTP server with WebCore... DONE");
    }
}
