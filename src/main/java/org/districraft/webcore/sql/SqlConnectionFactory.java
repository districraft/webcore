/*
 * This file is  licensed under the MIT License (MIT).
 *
 * Copyright (c) DistriCraft <https://www.districraft.org>
 * Copyright (c) contributors
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package org.districraft.webcore.sql;

import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * A factory for a DSLContext object
 *
 * @author "Jakub Sycha (utf_8x@districraft.org)"
 */
public class SqlConnectionFactory {
    private String mysql_host, mysql_username, mysql_password, mysql_database;
    private int mysql_port;
    private Connection conn;

    /**
     * Set the MySQL Host
     * @param host MySQL host
     * @return this
     */
    public SqlConnectionFactory host(String host) {
        this.mysql_host = host;
        return this;
    }

    /**
     * Close the connection
     * @throws SQLException
     */
    public void cleanup() throws SQLException {
        conn.close();
    }

    /**
     * Set the MySQL username
     * @param user MySQL Username
     * @return this
     */
    public SqlConnectionFactory username(String user) {
        this.mysql_username = user;
        return this;
    }

    /**
     * Set MySQL Password
     * @param password MySQL Password
     * @return this
     */
    public SqlConnectionFactory password(String password) {
        this.mysql_password = password;
        return this;
    }

    /**
     * Set the MySQL database name
     * @param db MySQL Database name
     * @return this
     */
    public SqlConnectionFactory database(String db) {
        this.mysql_database = db;
        return this;
    }

    /**
     * Set the MySQL port
     * @param port MySQL port
     * @return this
     */
    public SqlConnectionFactory port(int port) {
        this.mysql_port = port;
        return this;
    }

    /**
     * Construct the DSLContext object
     * @return DSLContext
     * @throws SQLException
     */
    public DSLContext build() throws SQLException {
        String url = String.format("jdbc:mysql://%s:%d/%s?serverTimezone=UTC", mysql_host, mysql_port, mysql_database);
        conn = DriverManager.getConnection(url, mysql_username, mysql_password);
        return DSL.using(conn, SQLDialect.MYSQL);
    }
}
