/*
 * This file is  licensed under the MIT License (MIT).
 *
 * Copyright (c) DistriCraft <https://www.districraft.org>
 * Copyright (c) contributors
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package org.districraft.webcore.http;

import org.districraft.webcore.annotation.AdminRequired;
import org.districraft.webcore.annotation.LoginRequired;
import org.districraft.webcore.annotation.Route;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * This class has to be extended by every controller
 *
 * @author "Jakub Sycha (utf_8x@districraft.org)"
 */
public class Controller extends HttpServlet {
    private Map<String, Map<String, Object>> mapping = new HashMap<>();

    public Controller() {
        for(Method m : this.getClass().getDeclaredMethods()) {
            Route routeAnnotation = m.getAnnotation(Route.class);
            LoginRequired loginAnnotation = m.getAnnotation(LoginRequired.class);
            AdminRequired adminAnnotation = m.getAnnotation(AdminRequired.class);

            if(routeAnnotation != null) {
                Map<String, Object> lmap = new HashMap<>();

                lmap.put("controller", m);

                if(loginAnnotation != null) {
                    lmap.put("login", true);
                } else {
                    lmap.put("login", false);
                }

                if(adminAnnotation != null) {
                    lmap.put("admin", true);
                } else {
                    lmap.put("admin", false);
                }

                mapping.put(routeAnnotation.value(), lmap);
            }
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String route = request.getPathInfo();

        if(route == null || route.equalsIgnoreCase("")) route = "/";



        Map<String, Object> mapped = mapping.get(route);

        if(mapped == null || mapped.getOrDefault("controller", null) == null) {
            response.setStatus(404);
            response.getWriter().println("404 - Server error");
            return;
        }

        try {

            if((boolean) mapped.getOrDefault("login", false)) {
                if(!performLoginChecks(request, response)) {
                    response.setStatus(403);
                    response.getWriter().println("403 - Unauthorized");
                    return;
                }
            }

            Method controller = (Method) mapped.get("controller");
            controller.invoke(this, request, new Response(response));
        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
            response.setStatus(500);
            response.getWriter().println("500 - Server error");
        }
    }

    private boolean performLoginChecks(HttpServletRequest request, HttpServletResponse response) {
        return true;
    }
}
