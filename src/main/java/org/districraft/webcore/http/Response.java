/*
 * This file is  licensed under the MIT License (MIT).
 *
 * Copyright (c) DistriCraft <https://www.districraft.org>
 * Copyright (c) contributors
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package org.districraft.webcore.http;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.jooq.tools.json.JSONObject;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author "Jakub Sycha (utf_8x@districraft.org)"
 */
public class Response {
    private HttpServletResponse responseObject;
    private int statusCode = 200;

    /**
     * @param response The HttpServletResponse object from Jetty
     */
    public Response(HttpServletResponse response) {
        this.responseObject = response;
    }

    /**
     * Set the response's HTTP status code
     * @param statusCode HTTP Status code
     */
    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    /**
     * Get the HTTP status code
     * @return HTTP status code
     */
    public int getStatusCode() {
        return this.statusCode;
    }

    /**
     * Convert the input object into JSON and append it to the response
     * (Works best with HashMaps)
     *
     * @param input Input Object
     * @throws IOException
     */
    public void renderJson(Object input) throws IOException {
        Gson s = new GsonBuilder().create();
        String payload = s.toJson(input);
        responseObject.getWriter().println(payload);
        responseObject.setContentType("application/json");
        responseObject.setStatus(statusCode);
    }
}
