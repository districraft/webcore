/*
 * This file is  licensed under the MIT License (MIT).
 *
 * Copyright (c) DistriCraft <https://www.districraft.org>
 * Copyright (c) contributors
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package org.districraft.webcore.util;

import org.districraft.webcore.sql.SqlConnectionFactory;

import java.sql.SQLException;

/**
 * Shutdown hook that ensures all MySQL connections are closed before exitting
 *
 * @author "Jakub Sycha (utf_8x@districraft.org)"
 */
public class ShutdownThread extends Thread {
    private SqlConnectionFactory factory;

    /**
     * @param factory
     */
    public ShutdownThread(SqlConnectionFactory factory) {
        this.factory = factory;
    }

    public void run() {
        try {
            factory.cleanup();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
