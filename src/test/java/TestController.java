import org.districraft.webcore.annotation.Route;
import org.districraft.webcore.http.Controller;
import org.districraft.webcore.http.Response;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public class TestController extends Controller {
    @Route("/")
    public void index(HttpServletRequest req, Response res) throws IOException {
        res.renderJson("yo");
    }
}
