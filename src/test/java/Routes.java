import org.districraft.webcore.util.MappingBuilder;

import java.util.Map;

import static org.districraft.webcore.util.MappingBuilder.route;

public class Routes {
    Routes() {
        route("/test/*", TestController.class);
    }

    Map<String, Class<?>> build() {
        return MappingBuilder.getAll();
    }
}
