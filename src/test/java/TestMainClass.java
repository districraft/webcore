import org.districraft.webcore.WebCore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import static org.hamcrest.Matchers.instanceOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class TestMainClass {
    @Test
    public void testMainMethodInitialization() throws IOException {
        Logger log = LoggerFactory.getLogger(WebCore.class);
        WebCore wc = null;
        try {
            wc = new WebCore(log, "webcore.properties.test");
            wc.start(new Routes().build());
        } catch (Exception e) {

            e.printStackTrace();
        }

        URL url = new URL("http://localhost:8090/test/");
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");
        con.setConnectTimeout(5000);
        con.setReadTimeout(5000);
        int status = con.getResponseCode();

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer content = new StringBuffer();
        while((inputLine = in.readLine()) != null) {
            content.append(inputLine);
        }

        con.disconnect();


        System.out.println(content);

        assertNotNull(wc);
        assertThat(wc, instanceOf(WebCore.class));
        assertEquals(200, status);
    }
}
